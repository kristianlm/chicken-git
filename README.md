# chicken-git

libgit2 bindings for CHICKEN Scheme.

## Install

Obviously, libgit2 is required: <http://github.com/libgit2/>.

Assuming you have that, installation should be straightforward:

    $ git clone git://bitbucket.org/evhan/chicken-git.git
    $ cd chicken-git
    $ chicken-install -test

This extension requires libgit2 0.23.0 and Chicken 4.8.0 or newer.

## API

The library is split into two modules, `git` and `libgit2`:

* `libgit2` is essentially just the libgit2 API, thinly wrapped. Most of
  the function signatures remain the same, with two exceptions:

  * Memory for out parameters and other structures that would typically
    go on the stack is allocated automatically.
  * Return values are checked where appropriate, signaling an exception of type
    `(exn git)` when nonzero.

* `git` is a higher-level interface around `libgit2`, providing
  record types for each libgit2 structure.

Documentation is available at <http://wiki.call-cc.org/egg/git>.

## Notes

Some functionality is not yet provided, such as custom backends, remotes and
reflog inspection. Obviously, patches are more than welcome.

## Contact

  * Evan Hanson <evhan@foldling.org>

## License

BSD. See LICENSE for details.
